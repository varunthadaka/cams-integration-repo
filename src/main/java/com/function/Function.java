package com.function;

import com.microsoft.azure.functions.ExecutionContext;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.ServiceBusQueueTrigger;
import com.function.CustomerRequest.*;
import com.function.CustomerRequestFormatter.*;


import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class Function {

    private static final String CAMS_CREATE_ACCOUNT_URL = "https://cams-uat3.interfloratest.co.uk/customer/createCustomerAccount";
    private static final String CAMS_USER_NAME = "ftdinc";
    private static final String CAMS_PASSWORD = "ftdinc";
 
    @FunctionName("commerceTools-to-cams-integration")
    public void run(
            @ServiceBusQueueTrigger(
            name = "message",
            queueName = "commercetoolscustomerqueue",
            connection = "ServiceBusConnection")
            String message,
            final ExecutionContext context) {
    
        context.getLogger().info("Messages Received from ServiceBusqueue: "+message);
        JSONObject jsonmessage = new JSONObject(message);  
        CustomerDetailsRequest Customerinfo = null;

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        String Email = null;        
        try {
            Customerinfo = mapper.readValue(jsonmessage.toString(), CustomerDetailsRequest.class);
            String check = Customerinfo.getType();

            if("CustomerCreated".equals(check)){
                Email =  Customerinfo.getCustomer().getEmail();
            } else {
                Email = Customerinfo.getEmail();
            }

            context.getLogger().info("CustomerDetailsRequest   mappped :{}" +Customerinfo.getCustomer());
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            String respose = sendPOST(Email);
            context.getLogger().info("Customer data after post request: {}" + respose);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    

    
    private  String sendPOST(String email) throws Exception {
        String result = "";
        HttpPost post = new HttpPost(CAMS_CREATE_ACCOUNT_URL);
        post.addHeader("x-password", CAMS_USER_NAME);
        post.addHeader("x-user-name", CAMS_PASSWORD);
        post.addHeader(HttpHeaders.CONTENT_TYPE, "application/xml");


        CustomerFullProfile cf = new CustomerFullProfile();
        Customer2 customer = new Customer2();
            customer.setChannelId("1");
            customer.setAlternateEmail(email);
            customer.setTitle("1");
            customer.setDomain("INTERFLORA_UK");
            customer.setDomainGroup("INTERFLORA_UK");
            customer.setWorkPhone("510-522-6788");
            customer.setWorkPhoneExt("0083");
            customer.setHomePhone("510-522-6788");
            customer.setMobilePhone("510-522-6788");
            customer.setFax("10001010");
            customer.setNickName("CommerceTool");
            customer.setEmailType("office");
            customer.setPhoneType("mobile");
            customer.setIsOnlineMyAccount("N");
            customer.setWebsiteURL("www.interflora.co.uk");
            customer.setMarketPreferences("Y");
            customer.setEmail(email);
            customer.setDomain("INTERFLORA_UK");
            customer.setDomainGroup("INTERFLORA_UK");
    
        CustomerMarketingPreferences cm = new CustomerMarketingPreferences();
    
            cm.setEmail("y");
            cm.setSms("y");
            cm.setPost("y");
            cm.setDigitalMarketing("N");
            cm.setOrigin("TEST");
            cm.setReference("TEST");
            cm.setMarketingCopyVersion(1);
            cm.setPrivacyPolicyVersion(1);
        customer.setCustomerMarketingPreferences(cm);
    
        CustomerAddress ca = new CustomerAddress();
            ca.setChannelId(1);
            ca.setCity("Hyderabad");
            ca.setCountry("IND");
            ca.setCountryCode("IN");
            ca.setState("andhra pradesh");
            ca.setTitle("Mr");
            ca.setFname("Commerce");
            ca.setLname("Toold");
            ca.setEmail(email);
            ca.setNickName("chand");
            ca.setZip("500072");
            ca.setDayPhone("510-522-6788");
            ca.setDayPhoneExt("4747");
            ca.setEveningPhone("510-522-6788");
            ca.setAddressType("DEFAULT");
            ca.setAddressPurpose("self");
            ca.setLatitude("103");
            ca.setLongitude("230");
            ca.setStreetAddress("somajiguda");
            ca.setAddress1("Floor 7, Babukhan Center INDIA");
            ca.setAddress2("test");
            ca.setFax("1023838333");
            ca.setDeliveryInstruction("delivery before 16pm");
            ca.setDeliveryInstructionOption("OTHER");
    
            cf.setCustomer(customer );

            ArrayList<CustomerAddress> cars = new ArrayList<CustomerAddress>();    
            cars.add(ca);
            cf.setCustomerAddressesList(cars);
            
            JAXBContext jaxbContext = JAXBContext.newInstance(CustomerFullProfile.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(cf, sw);
            String xmlContent = sw.toString();
            System.out.print( "Parsed xml content" + xmlContent );
            xmlContent.replace("CustomerFullProfile", " ");

          post.setEntity(new StringEntity(xmlContent));

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(post)) {

                result = EntityUtils.toString(response.getEntity());
                System.out.println("Response for cams   is " + result);
        
        }
        return result;
    }
}