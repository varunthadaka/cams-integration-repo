package com.function.CustomerRequestFormatter;

import java.util.Date;


public class CustomerMarketingPreferences {

	private Integer customerId;
	private String emailAddress;
	private String domain;
	private String email;	
	private String sms;	
	private String post;	
	private String digitalMarketing;
	private String origin;
	private String reference;
	private Integer marketingCopyVersion;
	private Integer privacyPolicyVersion;
	private String createdBy;
	private Date modifiedDate;
	
	private String websiteURL;
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}	

	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSms() {
		return sms;
	}
	public void setSms(String sms) {
		this.sms = sms;
	}
	public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}
	public String getDigitalMarketing() {
		return digitalMarketing;
	}
	public void setDigitalMarketing(String digitalMarketing) {
		this.digitalMarketing = digitalMarketing;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public Integer getMarketingCopyVersion() {
		return marketingCopyVersion;
	}
	public void setMarketingCopyVersion(Integer marketingCopyversion) {
		this.marketingCopyVersion = marketingCopyversion;
	}
	public Integer getPrivacyPolicyVersion() {
		return privacyPolicyVersion;
	}
	public void setPrivacyPolicyVersion(Integer privacyPolicyVersion) {
		this.privacyPolicyVersion = privacyPolicyVersion;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getWebsiteURL() {
		return websiteURL;
	}
	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}	
	@Override
	public String toString() {
		return "CustomerMarketingPreferences [customerId=" + customerId
				+ ", emailAddress=" + emailAddress + ", domain=" + domain
				+ ", email=" + email + ", sms=" + sms + ", post=" + post
				+ ", digitalMarketing=" + digitalMarketing + ", origin="
				+ origin + ", reference=" + reference
				+ ", marketingCopyVersion=" + marketingCopyVersion
				+ ", privacyPolicyVersion=" + privacyPolicyVersion
				+ ", createdBy=" + createdBy + ", modifiedDate=" + modifiedDate
				+ ", websiteURL=" + websiteURL + "]";
	}
}