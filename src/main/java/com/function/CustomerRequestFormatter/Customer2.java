package com.function.CustomerRequestFormatter;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class Customer2{
    	
    private Integer customerId;

    private Date modifiedDate;

    private Date createdDate;

    private String channelId;

    private String email;

    private String alternateEmail;

    private String fname;

    /**
     * holds the last name of the customer
     */
    private String lname;

    /**
     * holds the gender of the customer
     */
    private String gender;

    /**
     * holds the password
     */
    private String password;

    /**
     * holds the status of the customer
     */
    private String status;

    /**
     * holds the salt
     */
    private String salt;

    /**
     * holds the password challenging question
     */
    private String pwdChallengQuestion;

    /**
     * holds the password challenging answer
     */
    private String pwdChallengAnswer;

    /**
     * holds the domain where the user has tried
     */
    private String domain;

    /**
     * holds the flag to say that the customer information is shared across
     * different channels 
     */
    private String isShared;

    /**
     * holds the workphone 
     */
    private String workPhone;

    /**
     * holds the workphone extenstion of the customer
     */
    private String workPhoneExt;

    /**
     * holds the home phone of the customer
     */
    private String homePhone;
    
    /**
     * holds the mobile phone of the customer
     */
    private String mobilePhone;

    /**
     * holds the fax 
     */
    private String fax;

    /**
     * holds the time zone
     */
    private String timezone;
    
    /**
     * holds the invalid password flag
     */
    private String invalidPassword;   
    /**
     * This is used to hold the reason for blocklisting the user 
     */
    private String  blacklistReason;
    
    private String isOnlineMyAccount;
    
    private String nickName;
    
    private String emailType;
    
    private String phoneType;
    
    private String domainGroup;
    
    private String title;
    
    private String linkKey;
    
    private String resetString;
    
    private String websiteURL;
    
    private String marketPreferences;
    
    private String shopName;
    
    private String shopNumber;
    
    private CustomerMarketingPreferences customerMarketingPreferences;
    
    // setter and getter methods for all private class variables

    


	/**
	 * @return the customerId
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	public String getBlacklistReason() {
		return blacklistReason;
	}

	public void setBlacklistReason(String blacklistReason) {
		this.blacklistReason = blacklistReason;
	}

	/**
	 * @return the invalidPassword
	 */
	public String getInvalidPassword() {
		return invalidPassword;
	}

	/**
	 * @param invalidPassword the invalidPassword to set
	 */
	public void setInvalidPassword(String invalidPassword) {
		this.invalidPassword = invalidPassword;
	}
	
	
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the channelId
	 */
	public String getChannelId() {
		return channelId;
	}

	/**
	 * @param channelId the channelId to set
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the alternateEmail
	 */
	public String getAlternateEmail() {
		return alternateEmail;
	}

	/**
	 * @param alternateEmail the alternateEmail to set
	 */
	public void setAlternateEmail(String alternateEmail) {
		this.alternateEmail = alternateEmail;
	}

	/**
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}

	/**
	 * @param fname the fname to set
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}

	/**
	 * @return the lname
	 */
	public String getLname() {
		return lname;
	}

	/**
	 * @param lname the lname to set
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the salt
	 */
	public String getSalt() {
		return salt;
	}

	/**
	 * @param salt the salt to set
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}

	/**
	 * @return the pwdChallengQuestion
	 */
	public String getPwdChallengQuestion() {
		return pwdChallengQuestion;
	}

	/**
	 * @param pwdChallengQuestion the pwdChallengQuestion to set
	 */
	public void setPwdChallengQuestion(String pwdChallengQuestion) {
		this.pwdChallengQuestion = pwdChallengQuestion;
	}

	/**
	 * @return the pwdChallengAnswer
	 */
	public String getPwdChallengAnswer() {
		return pwdChallengAnswer;
	}

	/**
	 * @param pwdChallengAnswer the pwdChallengAnswer to set
	 */
	public void setPwdChallengAnswer(String pwdChallengAnswer) {
		this.pwdChallengAnswer = pwdChallengAnswer;
	}

	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * @param domain the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * @return the isShared
	 */
	public String getIsShared() {
		return isShared;
	}

	/**
	 * @param isShared the isShared to set
	 */
	public void setIsShared(String isShared) {
		this.isShared = isShared;
	}

	/**
	 * @return the workPhone
	 */
	public String getWorkPhone() {
		return workPhone;
	}

	/**
	 * @param workPhone the workPhone to set
	 */
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	/**
	 * @return the workPhoneExt
	 */
	public String getWorkPhoneExt() {
		return workPhoneExt;
	}

	/**
	 * @param workPhoneExt the workPhoneExt to set
	 */
	public void setWorkPhoneExt(String workPhoneExt) {
		this.workPhoneExt = workPhoneExt;
	}

	/**
	 * @return the homePhone
	 */
	public String getHomePhone() {
		return homePhone;
	}

	/**
	 * @param homePhone the homePhone to set
	 */
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param fax the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return the timezone
	 */
	public String getTimezone() {
		return timezone;
	}

	/**
	 * @param timezone the timezone to set
	 */
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getIsOnlineMyAccount() {
		return isOnlineMyAccount;
	}

	public void setIsOnlineMyAccount(String isOnlineMyAccount) {
		this.isOnlineMyAccount = isOnlineMyAccount;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public String getDomainGroup() {
		return domainGroup;
	}

	public void setDomainGroup(String domainGroup) {
		this.domainGroup = domainGroup;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLinkKey() {
		return linkKey;
	}

	public void setLinkKey(String linkKey) {
		this.linkKey = linkKey;
	}

	public String getResetString() {
		return resetString;
	}

	public void setResetString(String resetString) {
		this.resetString = resetString;
	}

	public String getWebsiteURL() {
		return websiteURL;
	}

	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}

	public String getMarketPreferences() {
		return marketPreferences;
	}

	public void setMarketPreferences(String marketPreferences) {
		this.marketPreferences = marketPreferences;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopNumber() {
		return shopNumber;
	}

	public void setShopNumber(String shopNumber) {
		this.shopNumber = shopNumber;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", modifiedDate="
				+ modifiedDate + ", createdDate=" + createdDate
				+ ", channelId=" + channelId + ", email=" + email
				+ ", alternateEmail=" + alternateEmail + ", fname=" + fname
				+ ", lname=" + lname + ", gender=" + gender + ", status=" + status + ", salt=" + salt
				+ ", pwdChallengQuestion=" + pwdChallengQuestion
				+ ", pwdChallengAnswer=" + pwdChallengAnswer + ", domain="
				+ domain + ", isShared=" + isShared + ", workPhone="
				+ workPhone + ", workPhoneExt=" + workPhoneExt + ", homePhone="
				+ homePhone + ", mobilePhone=" + mobilePhone + ", fax=" + fax
				+ ", timezone=" + timezone + ",  blacklistReason=" + blacklistReason
				+ ", isOnlineMyAccount=" + isOnlineMyAccount + ", nickName="
				+ nickName + ", emailType=" + emailType + ", phoneType="
				+ phoneType + ", domainGroup=" + domainGroup + ", title="
				+ title + ", linkKey=" + linkKey + ", resetString="
				+ resetString + ", websiteURL=" + websiteURL
				+ ", marketPreferences=" + marketPreferences + ", shopName="
				+ shopName + ", shopNumber=" + shopNumber
				+ ", customerMarketingPreferences=" + customerMarketingPreferences + "]";
	}

	@XmlElement
	public CustomerMarketingPreferences getCustomerMarketingPreferences() {
		return customerMarketingPreferences;
	}

	public void setCustomerMarketingPreferences(
			CustomerMarketingPreferences customerMarketingPreferences) {
		this.customerMarketingPreferences = customerMarketingPreferences;
	}
    
    
}