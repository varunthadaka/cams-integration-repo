package com.function.CustomerRequestFormatter;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.transform.Result;  

@XmlRootElement  
public class CustomerFullProfile implements Result {
       
	private Customer2 customer;
	
	private List<CustomerAddress> customerAddressesList;
	
	private String sendEmail;
	
    @XmlElement  
	public Customer2 getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer2 customer) {
		this.customer = customer;
	}

    @XmlElement  
	public List<CustomerAddress> getCustomerAddressesList() {
		return customerAddressesList;
	}

	public void setCustomerAddressesList(List<CustomerAddress> customerAddressesList) {
		this.customerAddressesList = customerAddressesList;
	}


    @XmlElement  
	public String getSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(String sendEmail) {
		this.sendEmail = sendEmail;
	}


	@Override
	public String toString() {
		return "CustomerFullProfile [customer=" + customer
				+ ", customerAddressesList=" + customerAddressesList
				+ ",  sendEmail=" + sendEmail
				+ "]";
	}

	@Override
	public void setSystemId(String systemId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getSystemId() {
		// TODO Auto-generated method stub
		return null;
	}	
}