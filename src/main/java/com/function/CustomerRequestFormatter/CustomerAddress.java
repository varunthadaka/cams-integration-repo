package com.function.CustomerRequestFormatter;

import java.util.Date;

public class CustomerAddress {
    
    
	
	/**
	 * holds the modified date of the customer address information
	 */
    private Date modifiedDate;

    /**
     * holds the created date of the customer address information
     */
    private Date createdDate;

    
    /**
     * holds the address id
     */
    private Integer addresseeId;

   /**
    * holds the customer id
    */
    private Integer customerId;

    /**
     * holds the channel id
     * 
     */
    private Integer channelId;

    /**
     * holds the city
     */
    private String city;

    
    /**
     * holds the country name
     */
    private String country;

    /**
     * holds the state
     */
    private String state;

    /**
     * holds the last name of the customer
     */
    private String lname;

    /**
     * holds the first name of the customer
     */
    private String fname;

    /**
     * holds the zip code of the customer
     */
    private String zip;

    /**
     * holds the day phone of the customer
     */
    private String dayPhone;

    /**
     * holds the day phone extenstion of the customer
     */
    private String dayPhoneExt;

    /**
     * holds the evening phone of the customer
     */
    private String eveningPhone;

    /**
     * holds the location name of the customer
     */
    private String locationName;

    /**
     * holds the status of the customer address
     * active address or inactive address
     */
    private String active;

    /**
     * holds the address type of the customer
     * 1.shipping 
     */
    private String addressType;
    
    /**
     * holds the purpose of the address
     */
    private String addressPurpose;

    /**
     * holds the verification status of the address
     * is the address is verified successfully or not
     */
    private Boolean verificationStatus;

    /**
     * This method is used to hold the
     * latitude of the address
     */
    private String latitude;

    /**
     * This method is used to hold the longitude of the 
     * address
     */
    private String longitude;

    /**
     * This method is used to store the
     * street address
     */
    private String streetAddress;

    /**
     * This is used to holds the address1 details
     */
    private String address1;

    /**
     * This method is used to holds the address2 details
     */
    private String address2;

    /**
     * This method is used to hold the fax details
     */
    private String fax;
    
    /**
     * This is used to hold the personal notes
     */
    private String personalNotes;
    
    private String title;
    
    private String prefFlorist;
    
    private String deliveryInstruction;
    
    private String deliveryInstructionOption;

    
    private String email;
    
    private String nickName;
    
    private String countryCode;
    
    // Setters and getters for private class variables

	/**
	 * @return the personalNotes
	 */
	public String getPersonalNotes() {
		return personalNotes;
	}

	/**
	 * @param personalNotes the personalNotes to set
	 */
	public void setPersonalNotes(String personalNotes) {
		this.personalNotes = personalNotes;
	}

	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the addresseeId
	 */
	public Integer getAddresseeId() {
		return addresseeId;
	}

	/**
	 * @param addresseeId the addresseeId to set
	 */
	public void setAddresseeId(Integer addresseeId) {
		this.addresseeId = addresseeId;
	}

	/**
	 * @return the customerId
	 */
	public Integer getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the channelId
	 */
	public Integer getChannelId() {
		return channelId;
	}

	/**
	 * @param channelId the channelId to set
	 */
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the lname
	 */
	public String getLname() {
		return lname;
	}

	/**
	 * @param lname the lname to set
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}

	/**
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}

	/**
	 * @param fname the fname to set
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the dayPhone
	 */
	public String getDayPhone() {
		return dayPhone;
	}

	/**
	 * @param dayPhone the dayPhone to set
	 */
	public void setDayPhone(String dayPhone) {
		this.dayPhone = dayPhone;
	}

	/**
	 * @return the dayPhoneExt
	 */
	public String getDayPhoneExt() {
		return dayPhoneExt;
	}

	/**
	 * @param dayPhoneExt the dayPhoneExt to set
	 */
	public void setDayPhoneExt(String dayPhoneExt) {
		this.dayPhoneExt = dayPhoneExt;
	}

	/**
	 * @return the eveningPhone
	 */
	public String getEveningPhone() {
		return eveningPhone;
	}

	/**
	 * @param eveningPhone the eveningPhone to set
	 */
	public void setEveningPhone(String eveningPhone) {
		this.eveningPhone = eveningPhone;
	}

	/**
	 * @return the locationName
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * @param locationName the locationName to set
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/**
	 * @return the active
	 */
	public String getActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(String active) {
		this.active = active;
	}

	/**
	 * @return the addressType
	 */
	public String getAddressType() {
		return addressType;
	}

	/**
	 * @param addressType the addressType to set
	 */
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	/**
	 * @return the addressPurpose
	 */
	public String getAddressPurpose() {
		return addressPurpose;
	}

	/**
	 * @param addressPurpose the addressPurpose to set
	 */
	public void setAddressPurpose(String addressPurpose) {
		this.addressPurpose = addressPurpose;
	}

	/**
	 * @return the verificationStatus
	 */
	public Boolean getVerificationStatus() {
		return verificationStatus;
	}

	/**
	 * @param verificationStatus the verificationStatus to set
	 */
	public void setVerificationStatus(Boolean verificationStatus) {
		this.verificationStatus = verificationStatus;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the streetAddress
	 */
	public String getStreetAddress() {
		return streetAddress;
	}

	/**
	 * @param streetAddress the streetAddress to set
	 */
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param fax the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPrefFlorist() {
		return prefFlorist;
	}

	public void setPrefFlorist(String prefFlorist) {
		this.prefFlorist = prefFlorist;
	}

	public String getDeliveryInstruction() {
		return deliveryInstruction;
	}

	public void setDeliveryInstruction(String deliveryInstruction) {
		this.deliveryInstruction = deliveryInstruction;
	}
	
	public String getDeliveryInstructionOption() {
		return deliveryInstructionOption;
	}

	public void setDeliveryInstructionOption(String deliveryInstructionOption) {
		this.deliveryInstructionOption = deliveryInstructionOption;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;

	}
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		return "CustomerAddress [modifiedDate=" + modifiedDate
				+ ", createdDate=" + createdDate + ", addresseeId="
				+ addresseeId + ", customerId=" + customerId + ", channelId="
				+ channelId + ", city=" + city + ", country=" + country
				+ ", state=" + state + ", lname=" + lname + ", fname=" + fname
				+ ", zip=" + zip + ", dayPhone=" + dayPhone + ", dayPhoneExt="
				+ dayPhoneExt + ", eveningPhone=" + eveningPhone
				+ ", locationName=" + locationName + ", active=" + active
				+ ", addressType=" + addressType + ", addressPurpose="
				+ addressPurpose + ", verificationStatus=" + verificationStatus
				+ ", latitude=" + latitude + ", longitude=" + longitude
				+ ", streetAddress=" + streetAddress + ", address1=" + address1
				+ ", address2=" + address2 + ", fax=" + fax
				+ ", personalNotes=" + personalNotes + ", title=" + title
				+ ", prefFlorist=" + prefFlorist + ", deliveryInstruction="
				+ deliveryInstruction + "email=" + email + ", nickName="
				+ nickName + ", countryCode=" + countryCode + "]";
	}
	

}