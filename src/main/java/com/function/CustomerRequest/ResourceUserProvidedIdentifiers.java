package com.function.CustomerRequest;
import com.fasterxml.jackson.annotation.JsonProperty;


public class ResourceUserProvidedIdentifiers {

    @JsonProperty("customerNumber")
    public String customerNumber;

    public String getCustomerNumber() {
        return customerNumber;
     }
     public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
     }
}
