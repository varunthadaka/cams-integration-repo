package com.function.CustomerRequest;
import com.fasterxml.jackson.annotation.JsonProperty;


public class CustomerDetailsRequest {
   
   @JsonProperty("notificationType")
    public String notificationType;

    @JsonProperty("projectKey")
    public String projectKey;

    @JsonProperty("id")
    public String id;

    @JsonProperty("version")
    public String version;

    @JsonProperty("sequenceNumber")
    public String sequenceNumber;

    @JsonProperty("resource")
    public Object resource;


    @JsonProperty("resourceVersion")
    public String resourceVersion;


    @JsonProperty("resourceUserProvidedIdentifiers")
    public ResourceUserProvidedIdentifiers resourceUserProvidedIdentifiers;

    @JsonProperty("type")
    public String type;

    @JsonProperty("customer")
    public Customer customer;

    @JsonProperty("email")
    public String email;

    @JsonProperty("createdAt")
    public String createdAt;

    @JsonProperty("lastModifiedAt")
    public String lastModifiedAt;


    public String getNotificationType() {
        return notificationType;
     }
     public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
     }

     public String getProjectKey() {
        return projectKey;
     }
     public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
     }

     public String getId() {
        return id;
     }
     public void setId(String id) {
        this.id = id;
     }
    
     public String getVersion() {
        return version;
     }
     public void setVersion(String version) {
        this.version = version;
     }

     public String getSequenceNumber() {
        return sequenceNumber;
     }
     public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
     }

     public Object getResource() {
        return resource;
     }
     public void setResource(Object resource) {
        this.resource = resource;
     }

     public String getResourceVersion() {
        return resourceVersion;
     }
     public void setResourceVersion(String resourceVersion) {
        this.resourceVersion = resourceVersion;
     }

     public ResourceUserProvidedIdentifiers getResourceUserProvidedIdentifiers() {
        return resourceUserProvidedIdentifiers;
     }

     public void setResourceUserProvidedIdentifiers(ResourceUserProvidedIdentifiers resourceUserProvidedIdentifiers) {
        this.resourceUserProvidedIdentifiers = resourceUserProvidedIdentifiers;
     }

     public String getType() {
        return type;
     }
     public void setType(String type) {
        this.type = type;
     }


     public Customer getCustomer() {
      return customer;
   }
   public Customer setCustomer(Customer customer) {
      return this.customer = customer;
   }

     public String getEmail() {
        return email;
     }
     public void setEmail(String email) {
        this.email = email;
     }

     public String getCreatedAt() {
        return createdAt;
     }
     public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
     }

     public String getLastModifiedAt() {
        return lastModifiedAt;
     }
     public void setLastModifiedAt(String lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
     }


}

