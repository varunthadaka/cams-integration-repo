package com.function.CustomerRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Customer {

    @JsonProperty("id")
    public String id;

    @JsonProperty("version")
    public String version;

    @JsonProperty("lastMessageSequenceNumber")
    public String lastMessageSequenceNumber;

    @JsonProperty("createdAt")
    public String createdAt;

    @JsonProperty("lastModifiedAt")
    public String lastModifiedAt;

   //  @JsonProperty("lastModifiedBy")
   //  public String lastModifiedBy;

   //  @JsonProperty("createdBy")
   //  public String createdBy;

    @JsonProperty("customerNumber")
    public String customerNumber;

    @JsonProperty("email")
    public String email;

    @JsonProperty("firstName")
    public String firstName;
   
    @JsonProperty("lastName")
    public String lastName;

    @JsonProperty("middleName")
    public String middleName;

    @JsonProperty("title")
    public String title;

    @JsonProperty("salutation")
    public String salutation;

    @JsonProperty("dateOfBirth")
    public String dateOfBirth;

    @JsonProperty("companyName")
    public String companyName;

    @JsonProperty("password")
    public String password;

    // @JsonProperty("addresses")
    // public String addresses;

    // @JsonProperty("shippingAddressIds")
    // public String shippingAddressIds;

    // @JsonProperty("billingAddressIds")
    // public String billingAddressIds;

    @JsonProperty("isEmailVerified")
    public String isEmailVerified;

    // @JsonProperty("stores")
    // public String stores;


    public String getId() {
        return id;
     }
     public void setId(String id) {
        this.id = id;
     }

     public String getVersion() {
        return version;
     }
     public void setVersion(String version) {
        this.version = version;
     }

     public String getLastMessageSequenceNumber() {
        return lastMessageSequenceNumber;
     }
     public void setLastMessageSequenceNumber(String lastMessageSequenceNumber) {
        this.lastMessageSequenceNumber = lastMessageSequenceNumber;
     }

     public String getCreatedAt() {
        return createdAt;
     }
     public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
     }


     public String getLastModifiedAt() {
        return lastModifiedAt;
     }
     public void setLastModifiedAt(String lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
     }


   //   public String getLastModifiedBy() {
   //      return lastModifiedBy;
   //   }
   //   public void setLastModifiedBy(String lastModifiedBy) {
   //      this.lastModifiedBy = lastModifiedBy;
   //   }



   //   public String getCreatedBy() {
   //      return createdBy;
   //   }
   //   public void setCreatedBy(String createdBy) {
   //      this.createdBy = createdBy;
   //   }


     public String getCustomerNumber() {
        return customerNumber;
     }
     public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
     }


     public String getEmail() {
        return email;
     }
     public void setEmail(String email) {
        this.email = email;
     }


     public String getFirstName() {
        return firstName;
     }
     public void setFirstName(String firstName) {
        this.firstName = firstName;
     }


     public String getLastName() {
        return lastName;
     }
     public void setLastName(String lastName) {
        this.lastName = lastName;
     }


     public String getMiddleName() {
        return middleName;
     }
     public void setMiddleName(String middleName) {
        this.middleName = middleName;
     }

     public String getTitle() {
        return title;
     }
     public void setTitle(String title) {
        this.title = title;
     }


     public String getSalutation() {
        return salutation;
     }
     public void setSalutation(String salutation) {
        this.salutation = salutation;
     }

     public String getDateOfBirth() {
        return dateOfBirth;
     }
     public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;


     }public String getCompanyName() {
        return companyName;
     }
     public void setCompanyName(String companyName) {
        this.companyName = companyName;
     }


     public String getPassword() {
        return password;
     }
     public void setPassword(String password) {
        this.password = password;
     }

    //  public String getAddresses() {
    //     return addresses;
    //  }
    //  public void setAddresses(String addresses) {
    //     this.addresses = addresses;
    //  }

    //  public String getShippingAddressIds() {
    //     return shippingAddressIds;
    //  }
    //  public void setShippingAddressIds(String shippingAddressIds) {
    //     this.shippingAddressIds = shippingAddressIds;
    //  }


    //  public String getBillingAddressIds() {
    //     return billingAddressIds;
    //  }
    //  public void setBillingAddressIds(String billingAddressIds) {
    //     this.billingAddressIds = billingAddressIds;
    //  }






}
